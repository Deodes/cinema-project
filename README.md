###  **CINEMA-PROJECT**

### Features

- JavaScript (ECMAScript 6) 
- HTML5
- CSS3
- Webpack
- LocalStorage

#  

### How to start?


`-npm install`

`-npm run build`

`-npm run serve` or you can just open 'dist/index.html' file in your browser

#  

### Authors

- PENCHEV VLADIMIR, DENIS KHISAMIEV

# 

### Organisations

- ANDERSEN COMPANY

# 

### About

CINEMA-PROJECT is a completely open source application. 
Includes all the necessary functionality for integration with a cinema network. 

### Allows you:
- book and buy tickets 
- simulate user authorizations 
- dependence of session time on current date and day
- hall scheme rendering 
- caching booking seats in local storage
and more. 
